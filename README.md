Our Ride-share web application could support almost all the functionalities that being described in the check list;
If you are a new user, you could sign up and it will redirect you to the login page after you finish the registration. Then you could log in.

After login, you could see several links are provided, and you could not see it before login. (Such as logout, profile, etc)
You could choose to register as a driver after login. It will redirect you to the driver registration page and it will ask you to provide your vehicle information, such as license plate number, your vehicle type, the maximum number of passengers it allows.

After the driver registration, if you try to register again, it will tell you that you have already registered as a driver and it will have a link below to begin searching the rider; we do not have another way to start a search for driver. You have to click the link for registering the driver, then the web will give your the search link.

You could view your personal and vehicle info via the profile; you could also edit these information as well.

You could request a ride by clicking the "Request a ride". You need to specify your destination, date, number of passengers, what type of cars you want, do you allow to share, etc. 

After submitting the request, you could view you request details via Your personal request. You could also edit or delete(cancel) your request before the driver confirms the ride.

The web also allows people to request a share ride, you need to specify your destination, date, number of passengers, what type of cars you want, do you allow to share as well. Note the special request must match with the owner of the ride.

After joining the ride, the request info of the owner of the ride will be updated, which will show who was joined in the ride.

The functionalities that are incomplete:
1. If a person request a share ride, he could not view this request details(means he could not edit or cancel order) after joining the ride. The reason is mentioned in the danger log.
2. Although we could let peole join a ride, we could not prevent them from joining the same ride multiple times.